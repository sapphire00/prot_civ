import 'package:flutter/material.dart';
import './Noticias.dart';
import './Servicios.dart';
import './Mapa.dart';
import './Documentos.dart';
import './Signup.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Protección Civil",
      theme: ThemeData(
        primaryColor: Colors.green,
      ),
      home: Signup()
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage>{
  int _counter = 0;
  int _currentIndex = 0;
  final List<Widget> _children = [
    Denuncia(),
    Ubi(),
    Docs(),
    Serv(),
  ];


  void _incrementCounter(){
    setState(() {
      _counter++;
    });
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("Prot. Civil"),
      ),
      body:
      _children[_currentIndex],

      bottomNavigationBar: BottomNavigationBar(

        onTap: onTapTapped,
        currentIndex: _currentIndex,
        type: BottomNavigationBarType.fixed,
        items: [

          BottomNavigationBarItem(
              icon: new Icon(Icons.phone_in_talk),
              title: Text('Denuncias')
          ),
          BottomNavigationBarItem(
              icon: new Icon(Icons.location_on),
              title: Text('Ubicaciones')
          ),
          BottomNavigationBarItem(
              icon: new Icon(Icons.insert_drive_file),
              title: Text('Documentos')
          ),
          BottomNavigationBarItem(
              icon: new Icon(Icons.people),
              title: Text('Servicios')
          ),
        ],
      ),
    );
  }

  void onTapTapped(int index){
    setState(() {
      _currentIndex = index;
    });
  }
}