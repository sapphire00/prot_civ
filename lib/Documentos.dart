import 'package:flutter/material.dart';
import 'package:cedulas_dashboard/Sets/Set_01/Docs_Set_01.dart';
import 'package:cedulas_dashboard/Sets/Set_02/Docs_Set_02.dart';
import 'package:cedulas_dashboard/Sets/Set_03/Docs_Set_03.dart';
import 'package:cedulas_dashboard/Sets/Set_04/Docs_Set_04.dart';
import 'package:cedulas_dashboard/Sets/Set_05/Docs_Set_05.dart';
import 'package:cedulas_dashboard/Sets/Set_06/Docs_Set_06.dart';
import 'package:cedulas_dashboard/Sets/Set_07/Docs_Set_07.dart';


class Docs extends StatelessWidget{
  @override
  Widget build(BuildContext context){
    return MaterialApp(
      home: Scaffold(
        body: ListView(
          padding: const EdgeInsets.all(2.0),
          children: [
            Container(
              decoration: BoxDecoration(
                border: Border(
                    top: BorderSide(color: Colors.deepOrangeAccent, width: 2.5,))
              ),
              margin: EdgeInsets.all(15.0),
              child: RaisedButton(
                textColor: Colors.black,
                color: Colors.yellow,
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) => Set_01()));
                },
                child: Text("Fenomenos Geologicos-Geomorfologicos"),
              ),
            ),
            Container(
              decoration: BoxDecoration(
                  border: Border(
                      top: BorderSide(color: Colors.deepOrangeAccent,width: 2.5))
              ),
              margin: EdgeInsets.all(15.0),
              child: RaisedButton(
                textColor: Colors.white,
                color: Colors.blue,
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) => Set_02()));
                },
                child: Text("Fenomenos Hidrometeorológicos"),
              ),
            ),
            Container(
              decoration: BoxDecoration(
                  border: Border(
                      top: BorderSide(color: Colors.deepOrangeAccent,width: 2.5))
              ),
              margin: EdgeInsets.all(15.0),
              child: RaisedButton(
                textColor: Colors.white,
                color: Colors.red,
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) => Set_03()));
                },
                child: Text("Químico-Tecnológicos"),
              ),
            ),
            Container(
              decoration: BoxDecoration(
                  border: Border(
                      top: BorderSide(color: Colors.deepOrangeAccent,width: 2.5))
              ),
              margin: EdgeInsets.all(15.0),
              child: RaisedButton(
                textColor: Colors.white,
                color: Colors.green,
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) => Set_04()));
                },
                child: Text("Fenomenos Sanitario-Ecológicos"),
              ),
            ),
            Container(
              decoration: BoxDecoration(
                  border: Border(
                      top: BorderSide(color: Colors.deepOrangeAccent,width: 2.5))
              ),
              margin: EdgeInsets.all(15.0),
              child: RaisedButton(
                textColor: Colors.white,
                color: Colors.black54,
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) => Set_05()));
                },
                child: Text("Fenomenos Socio-Organizativos"),
              ),
            ),
            Container(
              decoration: BoxDecoration(
                  border: Border(
                      top: BorderSide(color: Colors.deepOrangeAccent,width: 2.5))
              ),
              margin: EdgeInsets.all(15.0),
              child: RaisedButton(
                textColor: Colors.white,
                color: Colors.brown,
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) => Set_06()));
                },
                child: Text("Subsistema Afectable"),
              ),
            ),
            Container(
              decoration: BoxDecoration(
                  border: Border(
                      top: BorderSide(color: Colors.deepOrangeAccent,width: 2.5))
              ),
              margin: EdgeInsets.all(15.0),
              child: RaisedButton(
                textColor: Colors.white,
                color: Colors.orange,
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) => Set_07()));
                },
                child: Text("Subsistema Regulador"),
              ),
            ),
          ],
        ),
      ),
    );
  }
}