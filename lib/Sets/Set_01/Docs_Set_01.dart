import 'package:flutter/material.dart';
import 'dart:async';
import 'package:path_provider/path_provider.dart';
import 'package:dio/dio.dart';

class  Set_01 extends StatefulWidget{

  @override
  _Set_01State createState() => _Set_01State();
}

class _Set_01State extends State<Set_01> {
  final pdf_file = "https://firebasestorage.googleapis.com/v0/b/botonpanico.appspot.com/o/testpdf.pdf?alt=media&token=1c0a4584-db27-4f21-91fc-405d2593c147";

  bool downloading = false;

  var progressString = "";

  Future<void> downloadFile() async{
    Dio dio = Dio();

    try{
      var dir = await getApplicationDocumentsDirectory();

      await dio.download(pdf_file, "${dir.path}/dummyfile.pdf",
          onReceiveProgress: (rec, total){
        print("Rec: $rec , Total: $total");

        setState(() {
          downloading = true;
          progressString = ((rec/total)*100).toStringAsFixed(0) + "%";
        });
      });
    } catch(e){
      print(e);
    }

    setState((){
      downloading = false;
      progressString = "Completed";
    });
    print("Download Completed");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Geologicos-Geomorfologicos"),
      ),
      body: ListView(
        children: <Widget>[
          Card(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                const ListTile(
                  title: Text("Cavidades y Minas", style: TextStyle(fontStyle: FontStyle.normal, fontWeight: FontWeight.w700),),
                  subtitle: Text("Cédula para Asentamientos en Cavidades en el Subsuelo y en Minas a Cielo Abierto", style: TextStyle(fontStyle: FontStyle.italic, fontWeight: FontWeight.w400),),
                ),
                ButtonTheme.bar(
                  child: ButtonBar(
                    children: <Widget>[
                      FlatButton(
                        child: const Text("Llenar documento"),
                        onPressed: () {
                        },
                      ),
                      FlatButton(
                        child: const Text("Descargar PDF"),
                        onPressed: (){
                          downloadFile();
                        },
                      )
                    ],
                  ),
                ),
                Divider(color: Colors.deepOrangeAccent, indent: 40.0, height: 10.0,)
              ],
            ),
          ),
          Card(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                const ListTile(
                  title: Text("Remocion y Erosion", style: TextStyle(fontStyle: FontStyle.normal, fontWeight: FontWeight.w700),),
                  subtitle: Text("Cédula para Asentamientos en Pendientes y en Zonas Adyacentes con Problemas de Remoción o Erosión", style: TextStyle(fontStyle: FontStyle.italic, fontWeight: FontWeight.w400),),
                ),
                ButtonTheme.bar(
                  child: ButtonBar(
                    children: <Widget>[
                      FlatButton(
                        child: const Text("Llenar documento"),
                        onPressed: () {

                        },
                      ),
                      FlatButton(
                        child: const Text("Descargar PDF"),
                        onPressed: (){

                        },
                      )
                    ],
                  ),
                ),
                Divider(color: Colors.deepOrangeAccent, indent: 40.0, height: 10.0,)
              ],
            ),
          ),
          Card(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                const ListTile(
                  title: Text("Deslizamientos", style: TextStyle(fontStyle: FontStyle.normal, fontWeight: FontWeight.w700),),
                  subtitle: Text("Cédula para Deslizamientos de Tierra", style: TextStyle(fontStyle: FontStyle.italic, fontWeight: FontWeight.w400),),
                ),
                ButtonTheme.bar(
                  child: ButtonBar(
                    children: <Widget>[
                      FlatButton(
                        child: const Text("Llenar documento"),
                        onPressed: () {

                        },
                      ),
                      FlatButton(
                        child: const Text("Descargar PDF"),
                        onPressed: (){

                        },
                      )
                    ],
                  ),
                ),
                Divider(color: Colors.deepOrangeAccent, indent: 40.0, height: 10.0,)
              ],
            ),
          ),
          Card(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                const ListTile(
                  title: Text("Fallas y Humdimientos", style: TextStyle(fontStyle: FontStyle.normal, fontWeight: FontWeight.w700),),
                  subtitle: Text("Cédula para Fallas, Fracturas, Agrietamientos y Hundimientos", style: TextStyle(fontStyle: FontStyle.italic, fontWeight: FontWeight.w400),),
                ),
                ButtonTheme.bar(
                  child: ButtonBar(
                    children: <Widget>[
                      FlatButton(
                        child: const Text("Llenar documento"),
                        onPressed: () {

                        },
                      ),
                      FlatButton(
                        child: const Text("Descargar PDF"),
                        onPressed: (){

                        },
                      )
                    ],
                  ),
                ),
                Divider(color: Colors.deepOrangeAccent, indent: 40.0, height: 10.0,)
              ],
            ),
          )
        ],
      ),
    );
  }
}