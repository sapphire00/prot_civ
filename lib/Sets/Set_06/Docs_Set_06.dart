import 'package:flutter/material.dart';

class  Set_06 extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Subsistema Afectable"),
      ),
      body: ListView(
        children: <Widget>[
          Card(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                const ListTile(
                  title: Text("Centros Culturales", style: TextStyle(fontStyle: FontStyle.normal, fontWeight: FontWeight.w700),),
                ),
                ButtonTheme.bar(
                  child: ButtonBar(
                    children: <Widget>[
                      FlatButton(
                        child: const Text("Llenar documento"),
                        onPressed: () {

                        },
                      ),
                      FlatButton(
                        child: const Text("Descargar PDF"),
                        onPressed: (){

                        },
                      )
                    ],
                  ),
                ),
                Divider(color: Colors.deepOrangeAccent, indent: 40.0, height: 10.0,)
              ],
            ),
          ),
          Card(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                const ListTile(
                  title: Text("Centros Recreativos", style: TextStyle(fontStyle: FontStyle.normal, fontWeight: FontWeight.w700),),
                ),
                ButtonTheme.bar(
                  child: ButtonBar(
                    children: <Widget>[
                      FlatButton(
                        child: const Text("Llenar documento"),
                        onPressed: () {

                        },
                      ),
                      FlatButton(
                        child: const Text("Descargar PDF"),
                        onPressed: (){

                        },
                      )
                    ],
                  ),
                ),
                Divider(color: Colors.deepOrangeAccent, indent: 40.0, height: 10.0,)
              ],
            ),
          ),
          Card(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                const ListTile(
                  title: Text("Hospedaje", style: TextStyle(fontStyle: FontStyle.normal, fontWeight: FontWeight.w700),),
                ),
                ButtonTheme.bar(
                  child: ButtonBar(
                    children: <Widget>[
                      FlatButton(
                        child: const Text("Llenar documento"),
                        onPressed: () {

                        },
                      ),
                      FlatButton(
                        child: const Text("Descargar PDF"),
                        onPressed: (){

                        },
                      )
                    ],
                  ),
                ),
                Divider(color: Colors.deepOrangeAccent, indent: 40.0, height: 10.0,)
              ],
            ),
          ),
          Card(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                const ListTile(
                  title: Text("Instituciones Educativas y de Rehabilitación", style: TextStyle(fontStyle: FontStyle.normal, fontWeight: FontWeight.w700),),
                ),
                ButtonTheme.bar(
                  child: ButtonBar(
                    children: <Widget>[
                      FlatButton(
                        child: const Text("Llenar documento"),
                        onPressed: () {

                        },
                      ),
                      FlatButton(
                        child: const Text("Descargar PDF"),
                        onPressed: (){

                        },
                      )
                    ],
                  ),
                ),
                Divider(color: Colors.deepOrangeAccent, indent: 40.0, height: 10.0,)
              ],
            ),
          ),
          Card(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                const ListTile(
                  title: Text("Mercados Tianguis y Centrales de Abasto", style: TextStyle(fontStyle: FontStyle.normal, fontWeight: FontWeight.w700),),
                ),
                ButtonTheme.bar(
                  child: ButtonBar(
                    children: <Widget>[
                      FlatButton(
                        child: const Text("Llenar documento"),
                        onPressed: () {

                        },
                      ),
                      FlatButton(
                        child: const Text("Descargar PDF"),
                        onPressed: (){

                        },
                      )
                    ],
                  ),
                ),
                Divider(color: Colors.deepOrangeAccent, indent: 40.0, height: 10.0,)
              ],
            ),
          ),
          Card(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                const ListTile(
                  title: Text("Bares, Restaurantes y Centros Nocturnos", style: TextStyle(fontStyle: FontStyle.normal, fontWeight: FontWeight.w700),),
                ),
                ButtonTheme.bar(
                  child: ButtonBar(
                    children: <Widget>[
                      FlatButton(
                        child: const Text("Llenar documento"),
                        onPressed: () {

                        },
                      ),
                      FlatButton(
                        child: const Text("Descargar PDF"),
                        onPressed: (){

                        },
                      )
                    ],
                  ),
                ),
                Divider(color: Colors.deepOrangeAccent, indent: 40.0, height: 10.0,)
              ],
            ),
          ),
          Card(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                const ListTile(
                  title: Text("Templos", style: TextStyle(fontStyle: FontStyle.normal, fontWeight: FontWeight.w700),),
                ),
                ButtonTheme.bar(
                  child: ButtonBar(
                    children: <Widget>[
                      FlatButton(
                        child: const Text("Llenar documento"),
                        onPressed: () {

                        },
                      ),
                      FlatButton(
                        child: const Text("Descargar PDF"),
                        onPressed: (){

                        },
                      )
                    ],
                  ),
                ),
                Divider(color: Colors.deepOrangeAccent, indent: 40.0, height: 10.0,)
              ],
            ),
          ),
          Card(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                const ListTile(
                  title: Text("Tiendas de Autoservicio y Departamentales", style: TextStyle(fontStyle: FontStyle.normal, fontWeight: FontWeight.w700),),
                ),
                ButtonTheme.bar(
                  child: ButtonBar(
                    children: <Widget>[
                      FlatButton(
                        child: const Text("Llenar documento"),
                        onPressed: () {

                        },
                      ),
                      FlatButton(
                        child: const Text("Descargar PDF"),
                        onPressed: (){

                        },
                      )
                    ],
                  ),
                ),
                Divider(color: Colors.deepOrangeAccent, indent: 40.0, height: 10.0,)
              ],
            ),
          )
        ],
      ),
    );
  }
}