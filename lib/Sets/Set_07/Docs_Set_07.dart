import 'package:flutter/material.dart';

class  Set_07 extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Subsistema Regulador"),
      ),
      body: ListView(
        children: <Widget>[
          Card(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                const ListTile(
                  title: Text("Centros de Respuesta Inmediata y Auxilio", style: TextStyle(fontStyle: FontStyle.normal, fontWeight: FontWeight.w700),),
                ),
                ButtonTheme.bar(
                  child: ButtonBar(
                    children: <Widget>[
                      FlatButton(
                        child: const Text("Llenar documento"),
                        onPressed: () {

                        },
                      ),
                      FlatButton(
                        child: const Text("Descargar PDF"),
                        onPressed: (){

                        },
                      )
                    ],
                  ),
                ),
                Divider(color: Colors.deepOrangeAccent, indent: 40.0, height: 10.0,)
              ],
            ),
          ),
          Card(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                const ListTile(
                  title: Text("Infraestructura Básica y Servicios Vitales", style: TextStyle(fontStyle: FontStyle.normal, fontWeight: FontWeight.w700),),
                ),
                ButtonTheme.bar(
                  child: ButtonBar(
                    children: <Widget>[
                      FlatButton(
                        child: const Text("Llenar documento"),
                        onPressed: () {

                        },
                      ),
                      FlatButton(
                        child: const Text("Descargar PDF"),
                        onPressed: (){

                        },
                      )
                    ],
                  ),
                ),
                Divider(color: Colors.deepOrangeAccent, indent: 40.0, height: 10.0,)
              ],
            ),
          ),
          Card(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                const ListTile(
                  title: Text("Instituciones de Salud y Laboratorios Clínicos", style: TextStyle(fontStyle: FontStyle.normal, fontWeight: FontWeight.w700),),
                ),
                ButtonTheme.bar(
                  child: ButtonBar(
                    children: <Widget>[
                      FlatButton(
                        child: const Text("Llenar documento"),
                        onPressed: () {

                        },
                      ),
                      FlatButton(
                        child: const Text("Descargar PDF"),
                        onPressed: (){

                        },
                      )
                    ],
                  ),
                ),
                Divider(color: Colors.deepOrangeAccent, indent: 40.0, height: 10.0,)
              ],
            ),
          ),
          Card(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                const ListTile(
                  title: Text("Medios de Comunicación", style: TextStyle(fontStyle: FontStyle.normal, fontWeight: FontWeight.w700),),
                ),
                ButtonTheme.bar(
                  child: ButtonBar(
                    children: <Widget>[
                      FlatButton(
                        child: const Text("Llenar documento"),
                        onPressed: () {

                        },
                      ),
                      FlatButton(
                        child: const Text("Descargar PDF"),
                        onPressed: (){

                        },
                      )
                    ],
                  ),
                ),
                Divider(color: Colors.deepOrangeAccent, indent: 40.0, height: 10.0,)
              ],
            ),
          )
        ],
      ),
    );
  }
}