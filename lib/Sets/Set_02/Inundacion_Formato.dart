import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:geolocator/geolocator.dart';
import 'package:firebase_database/firebase_database.dart';
import 'dart:async';


class Inun_Formato extends StatefulWidget{
  _Inun_FormatoState createState() => _Inun_FormatoState();
}





class _Inun_FormatoState extends State<Inun_Formato>{

  final _formKeyHidro = GlobalKey<FormState>();
  static DateTime selectedDate = DateTime.now();
  String formattedDate = DateFormat('yyyy-MM-dd').format(selectedDate);
  String formattedDate2 = DateFormat('yyyy-MM-dd').format(selectedDate);
  String _tipoElegido;
  List<String> _tipos = ["Fluvial", "Lacustre", "Pluvial"];
  String _causaElegida;
  List<String> _causas = ["Asolve en el drenaje", "Red de drenaje insuficiente", "Excesiva cantidad de precipitacion en poco tiempo",
    "Terreno con poca pendiente", "Terreno poco permeable", "Zonas de origen lacustre", "Falla en la infraestructura hidraulica",
    "Zonas bajas sin drenaje natural", "Otras",];
  String _infraElegida;
  List<String> _infra = ["Carreteras", "Puentes", "Red de agua potable", "Red de drenaje", "Red telefónica",
    "Red elétrica", "Presa", "Bordo", "Canal", "Otras"];





Future<Null> _selectDate(BuildContext context) async {
  final DateTime picked = await showDatePicker(
    context: context,
    initialDate: selectedDate,
    firstDate: DateTime(1910, 8),
    lastDate: DateTime(2101));
    if ( picked != null && picked != selectedDate)
    setState(() {
      selectedDate=picked;
      formattedDate = DateFormat('yyyy-MM-dd').format(selectedDate);
    });
}

Widget _buildOtrosInput(){
  if( _causaElegida == 'Otras'){
    return TextFormField(decoration: InputDecoration(labelText: 'Especifique'),);
  } else {
    return Padding(padding: EdgeInsets.all(0.0),);
  }
}

void _mostrarSnackbar(BuildContext context){
  final snackBar = SnackBar(
    content: Text('Enviado'),
    action: SnackBarAction(
      label: 'Cerrar',
      onPressed: () {
        Scaffold.of(context).removeCurrentSnackBar();
      },
    ),
  );

  Scaffold.of(context).showSnackBar(snackBar);
}

void _mostrarSnackbar_photo(BuildContext context){
  final snackBar = SnackBar(
    content: Text('Foto Adjunta'),
    action: SnackBarAction(
      label: 'Cerrar',
      onPressed: () {
        Scaffold.of(context).removeCurrentSnackBar();
      },
    ),
  );

  Scaffold.of(context).showSnackBar(snackBar);
}


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Cedula para Inundación"),
      ),
      
      
      body: 
      Builder(
        builder: (context) =>
      SingleChildScrollView(
        child: Container(

          padding: EdgeInsets.all(16.0),
          child:

          Form(
            key: _formKeyHidro,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row
                  (children: <Widget>[
                  Flexible(
                    flex: 3,
                    child:
                    TextFormField(
                      decoration: InputDecoration(labelText: 'Fecha de Llenado'),
                      enabled: false,
                      controller: TextEditingController(text: '$formattedDate'),
                    ),
                  ),
                  Flexible(
                      child:
                      Container(margin: EdgeInsets.symmetric(horizontal: 4.0 ),
                        decoration: BoxDecoration(color: Colors.blueGrey),
                        child: IconButton(icon: Icon(Icons.calendar_today), onPressed: () => _selectDate(context),
                        ),
                      )
                  )
                ],),

                Padding(
                  padding: EdgeInsets.all(8.0)
                ),

                Row(
                  children: <Widget>[
                    Flexible(
                      flex: 3,
                      child: TextFormField(
                        decoration: InputDecoration(labelText: 'Nombre de Responsable'),
                      ),
                    ),
                    Flexible(
                      flex: 1,
                        child: TextFormField(
                          decoration: InputDecoration(labelText: "ZU"),
                        ))
                  ],
                ),

                Padding(
                    padding: EdgeInsets.all(8.0)
                ),

                ListTile(
                  title: Text("Datos Generales:",style: TextStyle(fontWeight: FontWeight.w700),),
                ),
                Row(children: <Widget>[
                  Flexible(
                    flex: 2,
                      child: TextFormField(
                        decoration: InputDecoration(labelText: "Municipio"),
                      ),),
                  Flexible(
                      flex: 1,
                        child: TextFormField(
                        decoration: InputDecoration(labelText: "Clave Mpio."),))
                ],),
                Row(children: <Widget>[
                  Flexible(
                    flex: 2,
                    child: TextFormField(
                      decoration: InputDecoration(labelText: "Localidad"),
                    ),),
                  Flexible(
                      flex: 1,
                      child: TextFormField(
                        decoration: InputDecoration(labelText: "Clave Loc"),))
                ],),
                Row(children: <Widget>[
                  Flexible(
                    flex: 2,
                    child: TextFormField(
                      decoration: InputDecoration(labelText: "Sublocalidad"),
                    ),),
                  Flexible(
                      flex: 1,
                      child: TextFormField(
                        decoration: InputDecoration(labelText: "Clave Subloc."),))
                ],),

                Padding(
                    padding: EdgeInsets.all(8.0)
                ),

                ListTile(
                  title: Text("Datos Específicos:",style: TextStyle(fontWeight: FontWeight.w700),),
                ),
                Wrap(
                  direction: Axis.vertical,
                  spacing: 10.0,
                  children: <Widget>[
                    DropdownButton<String>(
                        hint: Text("Tipo de Inundación"),
                        value: _tipoElegido,
                        items: _tipos.map((tipos){
                          return new DropdownMenuItem<String>(
                            value: tipos,
                            child: new Text(tipos),
                          );

                        }).toList(),
                        onChanged: (_) {
                          setState(() {
                            _tipoElegido = (_);
                          });
                        },
                    ),

                    DropdownButton<String>(
                        hint: Text("Causas"),
                        value: _causaElegida,
                        items: _causas.map((causas){
                          return new DropdownMenuItem<String>(
                            value: causas,
                            child: new Text(causas),
                          );

                        }).toList(),
                        onChanged: (_) {
                          setState(() {
                            _causaElegida = (_);
                          });
                        },
                    ),
                    SizedBox.shrink(child:
                    TextFormField(decoration: InputDecoration(labelText: 'Especifique'),),
                    ),
                    DropdownButton<String>(
                      hint: Text("Infraestructura afectada"),
                      value: _infraElegida,
                      items: _infra.map((infraestructuras){
                        return new DropdownMenuItem<String>(
                          value: infraestructuras,
                          child: new Text(infraestructuras),
                        );
                      }).toList(),
                      onChanged: (_) {
                        setState(() {
                          _infraElegida = (_);
                        });
                      },
                    ),
                  ],
                ),
                Row(
                  children: <Widget>[
                    Flexible(
                      flex: 1,
                        child: SizedBox(
                          child: ListTile(
                            title: Text("Zonas agricolas afectadas"),
                          ),
                        ),
                    ),
                    Flexible(
                      flex: 1,
                        child: TextFormField(decoration: InputDecoration(labelText: "No. de Hectareas"),
                        ),
                    )
                  ],
                ),
                 Row(
                      children: <Widget>[
                        Flexible(
                          flex: 1,
                          child: TextFormField(
                            enabled: false, 
                            decoration: InputDecoration(labelText: 'Fecha del Evento'),
                            controller: TextEditingController(text: '$formattedDate2'),
                            
                            ),
                        ),
                        Flexible(
                          flex: 1,
                          child: Container(margin: EdgeInsets.symmetric(horizontal: 4.0 ),
                        decoration: BoxDecoration(color: Colors.blueGrey),
                        child: IconButton(icon: Icon(Icons.calendar_today), onPressed: () => _selectDate(context),
                        ),
                      ),
                        )
                        
                      ],
                    ),
                    Row(
                      
                      children: <Widget>[
                        Flexible(
                          flex: 1,
                          child: TextFormField(
                             
                            decoration: InputDecoration(labelText: 'Nivel Máximo (metros)'),
                            
                            ),
                        ),
                        
                      ],
                    ),
                    Row(
                    children: <Widget>[
                      Flexible(
                        flex: 1,
                        child: 
                        TextFormField(
                          decoration: InputDecoration(labelText: 'No. Viviendas'),
                          ),
                      ),
                      Flexible(
                        flex: 1,
                        child: 
                        TextFormField(
                          decoration: InputDecoration(labelText: 'No. Muertos'),
                          ),
                      ),
                      Flexible(
                        flex: 1,
                        child: 
                        TextFormField(
                          decoration: InputDecoration(labelText: 'No. Viviendas'),
                          ),
                      ),
                    ],),
                    Row(children: <Widget>[
                    Flexible(
                      flex: 1,
                        child: 
                        TextFormField(
                          decoration: InputDecoration(labelText: 'No. Heridos'),
                          ),
                      ),
                    Flexible(
                      flex: 1,
                        child: 
                        TextFormField(
                          decoration: InputDecoration(labelText: 'No. Desaparecidos', labelStyle: TextStyle(fontSize: 12)),
                          ),
                      ),
                    Flexible(
                      flex: 1,
                        child: 
                        TextFormField(
                          decoration: InputDecoration(labelText: 'No. Damnificados', labelStyle: TextStyle(fontSize: 12)),
                          ),
                      ),
                    ],
                    
                    ),
            
                    Row(children: <Widget>[
                      Flexible(
                        flex:1,
                        child: Text('Superficie Afectada'),
                      ),
                      Padding(padding: EdgeInsets.all(8.0),),
                      Flexible(
                        flex: 1,
                        child: TextFormField(decoration: InputDecoration(labelText: 'No. Hectáreas'),),
                      )
                    ],),
                
                
                


                Padding(
                    padding: EdgeInsets.all(8.0)
                ),

                ListTile(
                  title: Text("Ubicación Cartográfica:",style: TextStyle(fontWeight: FontWeight.w700),),
                  subtitle: Text("Selección de ubicacion del suceso", style: TextStyle(fontStyle: FontStyle.italic, fontWeight: FontWeight.w400),),
                ),
                SizedBox(
                  height: 185.0,
                  child: GoogleMap(
                    markers: _markers,
                    mapType: MapType.hybrid,
                    initialCameraPosition: _kGooglePlex,
                    onMapCreated: (GoogleMapController controller){
                      _controller.complete(controller);
                    },
                  ),
                ),
                RaisedButton(
                  onPressed: (){
                    _getLocation();
                  },
                  child: Text("Ir a Ubicación Actual"),
                  ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 15.0),
                  child: RaisedButton(
                    onPressed: (){
                      
                      _mostrarSnackbar_photo(context);
                    },
                    child: Text("Adjuntar Fotografía"),
                  ),
                ),
                 Padding(
                  padding: const EdgeInsets.symmetric(vertical: 15.0),
                  child: RaisedButton(
                    onPressed: (){
                      if (_formKeyHidro.currentState.validate()){
                        _mostrarSnackbar(context);
                      }
                    },
                    child: Text("Confirmar Ubicación"),
                  ),
                )

              ],
            ),
          ),
        )
      )
      )
    );
  }
  static Position currentLocation;
  var geolocator = Geolocator();
  var locationOptions = LocationOptions(accuracy: LocationAccuracy.high, distanceFilter: 10);
  final Set<Marker> _markers = {};
  Future<Position> _getLocation() async {
    try{
      final GoogleMapController controller = await _controller.future;
      Position position = await Geolocator().getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
      currentLocation = position;
      print(position.latitude);
      print(position.longitude);
      controller.animateCamera(CameraUpdate.newCameraPosition(_kPos));
      _addMarker();

    } catch (e) {
      debugPrint(e);
      currentLocation = null;
    }


  }

  Future<Position> _getLiveLocation() async {
    final GoogleMapController controller = await _controller.future;
    StreamSubscription<Position> positionStream = geolocator.getPositionStream(locationOptions).listen(
            (Position position) {
          print(position == null ? 'Unknown' : position.latitude.toString() + ', ' + position.longitude.toString());
          controller.animateCamera(CameraUpdate.newCameraPosition(_kPos));
        });
    print(positionStream);
  }


  void _onMapCreated(GoogleMapController controller){
    _controller.complete(controller);
  }


  void _addMarker() {
    setState(() {
      _markers.add(Marker(
        markerId:MarkerId(currentLocation.toString()),
        position: LatLng(currentLocation.latitude, currentLocation.longitude),
        infoWindow: InfoWindow( title: 'Posicion Actual'),
        icon: BitmapDescriptor.defaultMarker,

      ));
    });
    print(currentLocation.toString());
  }


  Completer<GoogleMapController> _controller = Completer();

  static final CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(0, 0),

  );


  static final CameraPosition _kPos = CameraPosition(

    target: LatLng(currentLocation.latitude, currentLocation.longitude),
    zoom: 17.0,
  );


  final _formKey = GlobalKey<FormState>();
  final FirebaseDatabase _database = FirebaseDatabase.instance;
  final _textEditingController = TextEditingController();
}