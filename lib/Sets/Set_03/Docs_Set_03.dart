import 'package:flutter/material.dart';

class  Set_03 extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Quimico-Tecnologicos"),
      ),
      body: ListView(
        children: <Widget>[
          Card(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                const ListTile(
                  title: Text("Actividad Comercial", style: TextStyle(fontStyle: FontStyle.normal, fontWeight: FontWeight.w700),),
                ),
                ButtonTheme.bar(
                  child: ButtonBar(
                    children: <Widget>[
                      FlatButton(
                        child: const Text("Llenar documento"),
                        onPressed: () {

                        },
                      ),
                      FlatButton(
                        child: const Text("Descargar PDF"),
                        onPressed: (){

                        },
                      )
                    ],
                  ),
                ),
                Divider(color: Colors.deepOrangeAccent, indent: 40.0, height: 10.0,)
              ],
            ),
          ),
          Card(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                const ListTile(
                  title: Text("Estaciones de Servicio", style: TextStyle(fontStyle: FontStyle.normal, fontWeight: FontWeight.w700),),
                ),
                ButtonTheme.bar(
                  child: ButtonBar(
                    children: <Widget>[
                      FlatButton(
                        child: const Text("Llenar documento"),
                        onPressed: () {

                        },
                      ),
                      FlatButton(
                        child: const Text("Descargar PDF"),
                        onPressed: (){

                        },
                      )
                    ],
                  ),
                ),
                Divider(color: Colors.deepOrangeAccent, indent: 40.0, height: 10.0,)
              ],
            ),
          ),
          Card(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                const ListTile(
                  title: Text("Gaseras y Estaciones", style: TextStyle(fontStyle: FontStyle.normal, fontWeight: FontWeight.w700),),
                  subtitle: Text("Cédula para Gaseras y Estaciones de Carburación", style: TextStyle(fontStyle: FontStyle.italic, fontWeight: FontWeight.w400),),
                ),
                ButtonTheme.bar(
                  child: ButtonBar(
                    children: <Widget>[
                      FlatButton(
                        child: const Text("Llenar documento"),
                        onPressed: () {

                        },
                      ),
                      FlatButton(
                        child: const Text("Descargar PDF"),
                        onPressed: (){

                        },
                      )
                    ],
                  ),
                ),
                Divider(color: Colors.deepOrangeAccent, indent: 40.0, height: 10.0,)
              ],
            ),
          ),
          Card(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                const ListTile(
                  title: Text("Industrias", style: TextStyle(fontStyle: FontStyle.normal, fontWeight: FontWeight.w700),),
                ),
                ButtonTheme.bar(
                  child: ButtonBar(
                    children: <Widget>[
                      FlatButton(
                        child: const Text("Llenar documento"),
                        onPressed: () {

                        },
                      ),
                      FlatButton(
                        child: const Text("Descargar PDF"),
                        onPressed: (){

                        },
                      )
                    ],
                  ),
                ),
                Divider(color: Colors.deepOrangeAccent, indent: 40.0, height: 10.0,)
              ],
            ),
          ),
          Card(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                const ListTile(
                  title: Text("Polvorines", style: TextStyle(fontStyle: FontStyle.normal, fontWeight: FontWeight.w700),),
                ),
                ButtonTheme.bar(
                  child: ButtonBar(
                    children: <Widget>[
                      FlatButton(
                        child: const Text("Llenar documento"),
                        onPressed: () {

                        },
                      ),
                      FlatButton(
                        child: const Text("Descargar PDF"),
                        onPressed: (){

                        },
                      )
                    ],
                  ),
                ),
                Divider(color: Colors.deepOrangeAccent, indent: 40.0, height: 10.0,)
              ],
            ),
          ),
          Card(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                const ListTile(
                  title: Text("Listado de sustancias que por sus características hacen una actividad altamente riesgosa", style: TextStyle(fontStyle: FontStyle.normal, fontWeight: FontWeight.w700),),
                ),
                ButtonTheme.bar(
                  child: ButtonBar(
                    children: <Widget>[
                      FlatButton(
                        child: const Text("Llenar documento"),
                        onPressed: () {

                        },
                      ),
                      FlatButton(
                        child: const Text("Descargar PDF"),
                        onPressed: (){

                        },
                      )
                    ],
                  ),
                ),
                Divider(color: Colors.deepOrangeAccent, indent: 40.0, height: 10.0,)
              ],
            ),
          ),
          Card(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                const ListTile(
                  title: Text("Catálogo de la clasificación Mexicana de Actividades y Productos de Sector 3", style: TextStyle(fontStyle: FontStyle.normal, fontWeight: FontWeight.w700),),
                ),
                ButtonTheme.bar(
                  child: ButtonBar(
                    children: <Widget>[
                      FlatButton(
                        child: const Text("Llenar documento"),
                        onPressed: () {

                        },
                      ),
                      FlatButton(
                        child: const Text("Descargar PDF"),
                        onPressed: (){

                        },
                      )
                    ],
                  ),
                ),
                Divider(color: Colors.deepOrangeAccent, indent: 40.0, height: 10.0,)
              ],
            ),
          ),
          Card(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                const ListTile(
                  title: Text("Instructivo de llenado para Cédula de Industrias", style: TextStyle(fontStyle: FontStyle.normal, fontWeight: FontWeight.w700),),
                ),
                ButtonTheme.bar(
                  child: ButtonBar(
                    children: <Widget>[
                      FlatButton(
                        child: const Text("Llenar documento"),
                        onPressed: () {

                        },
                      ),
                      FlatButton(
                        child: const Text("Descargar PDF"),
                        onPressed: (){

                        },
                      )
                    ],
                  ),
                ),
                Divider(color: Colors.deepOrangeAccent, indent: 40.0, height: 10.0,)
              ],
            ),
          )
        ],
      ),
    );
  }
}