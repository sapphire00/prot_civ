import 'package:flutter/material.dart';

class  Set_05 extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Socio-Organizativos"),
      ),
      body: ListView(
        children: <Widget>[
          Card(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                const ListTile(
                  title: Text("Derechos de Via", style: TextStyle(fontStyle: FontStyle.normal, fontWeight: FontWeight.w700),),
                  subtitle: Text("Cédula para Asentamientos sobre Derechos de Vía", style: TextStyle(fontStyle: FontStyle.italic, fontWeight: FontWeight.w400),),
                ),
                ButtonTheme.bar(
                  child: ButtonBar(
                    children: <Widget>[
                      FlatButton(
                        child: const Text("Llenar documento"),
                        onPressed: () {

                        },
                      ),
                      FlatButton(
                        child: const Text("Descargar PDF"),
                        onPressed: (){

                        },
                      )
                    ],
                  ),
                ),
                Divider(color: Colors.deepOrangeAccent, indent: 40.0, height: 10.0,)
              ],
            ),
          )
        ],
      ),
    );
  }
}