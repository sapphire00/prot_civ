import 'package:flutter/material.dart';
import 'dart:async';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:geolocator/geolocator.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_core/firebase_core.dart';

class Ubi extends StatefulWidget {
  @override
  _UbiState createState() => _UbiState();
}

class _UbiState extends State<Ubi> {
  @override
  Widget build(BuildContext context){
    return Scaffold(
      body: Column(
        children: <Widget>[
          SizedBox(
            height: 450.0,
            child: GoogleMap(
              markers: _markers,
              mapType: MapType.hybrid,
              initialCameraPosition: _kGooglePlex,
              onMapCreated: (GoogleMapController controller){
                _controller.complete(controller);
              },
            ),
          ),
          RaisedButton(
            onPressed: (){
              _getLocation();
            },
            child: Text("Ir a Ubicacion Actual"),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 15.0),
            child: RaisedButton(
              onPressed: (){
                if (_formKey.currentState.validate()){
                  Scaffold.of(context)
                      .showSnackBar(SnackBar(content: Text("Enviado")));
                }
              },
              child: Text("Levantar Reporte"),
            ),
          ),
        ],
      ),
    );
  }

  static Position currentLocation;
  var geolocator = Geolocator();
  var locationOptions = LocationOptions(accuracy: LocationAccuracy.high, distanceFilter: 10);
  final Set<Marker> _markers = {};
  Future<Position> _getLocation() async {
    try{
      final GoogleMapController controller = await _controller.future;
      Position position = await Geolocator().getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
      currentLocation = position;
      print(position.latitude);
      print(position.longitude);
      controller.animateCamera(CameraUpdate.newCameraPosition(_kPos));
      _addMarker();

    } catch (e) {
      debugPrint(e);
      currentLocation = null;
    }


  }

  Future<Position> _getLiveLocation() async {
    final GoogleMapController controller = await _controller.future;
    StreamSubscription<Position> positionStream = geolocator.getPositionStream(locationOptions).listen(
            (Position position) {
          print(position == null ? 'Unknown' : position.latitude.toString() + ', ' + position.longitude.toString());
          controller.animateCamera(CameraUpdate.newCameraPosition(_kPos));
        });
    print(positionStream);
  }


  void _onMapCreated(GoogleMapController controller){
    _controller.complete(controller);
  }


  void _addMarker() {
    setState(() {
      _markers.add(Marker(
        markerId:MarkerId(currentLocation.toString()),
        position: LatLng(currentLocation.latitude, currentLocation.longitude),
        infoWindow: InfoWindow( title: 'Posicion Actual'),
        icon: BitmapDescriptor.defaultMarker,

      ));
    });
    print(currentLocation.toString());
  }


  Completer<GoogleMapController> _controller = Completer();

  static final CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(0, 0),

  );


  static final CameraPosition _kPos = CameraPosition(

    target: LatLng(currentLocation.latitude, currentLocation.longitude),
    zoom: 17.0,
  );


  final _formKey = GlobalKey<FormState>();
  final FirebaseDatabase _database = FirebaseDatabase.instance;
  final _textEditingController = TextEditingController();
}