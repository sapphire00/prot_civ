import 'package:cedulas_dashboard/main.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class Signup extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: "Login PC Demo",
      theme: new ThemeData(
        primaryColor: Colors.cyan
      ),
      home: new LoginPage(),
    );
  }
}

class LoginPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new _LoginPageState();
}

enum FormType{
  login,
  register
}

class _LoginPageState extends State<LoginPage> {
  String _selectedGender;
  List<String> _genders = ['Hombre', 'Mujer'];
  static DateTime selectedDate = DateTime.now();
  String formattedDate = DateFormat('yyyy-MM-dd').format(selectedDate);
  final TextEditingController _dateController = new TextEditingController();
  final TextEditingController _emailFilter = new TextEditingController();
  final TextEditingController _passwordFilter = new TextEditingController();
  String _email = "";
  String _password = "";
  FormType _form = FormType.register;

  _LoginPageState(){
    _emailFilter.addListener(_emailListen);
    _passwordFilter.addListener(_passwordListen);
  }

  Future<Null> _selectDate(BuildContext context) async{
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(1990, 1),
        lastDate: DateTime(2120));
    if (picked !=null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
        formattedDate = DateFormat("yyyy-MM-dd").format(selectedDate);
      });
  }

  void _emailListen() {
    if (_emailFilter.text.isEmpty){
      _email = "";
    } else {
      _email = _emailFilter.text;
    }
  }

  void _passwordListen() {
    if (_passwordFilter.text.isEmpty) {
      _password = "";
    } else {
      _password = _passwordFilter.text;
    }
  }

  void _dateListen(){

  }

  void _formChange () async {
    setState(() {
      if (_form == FormType.register) {
        _form = FormType.login;
      } else {
        _form = FormType.register;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: _buildBar(context),
      body:
        new SingleChildScrollView(child:
          new Container(
            padding: EdgeInsets.all(16.0),
            child: new Column(

              children: <Widget>[
                _buildTextFields(),
                _buildButtons(),
              ],
            ),
          ),
        ),
    );
  }

  Widget _buildBar(BuildContext context){
    if (_form == FormType.login){
      return new AppBar(
        title: new Text("Iniciar Seción"),
        centerTitle: true,
      );
    } else {
      return new AppBar(
        title: new Text("Crear Cuenta"),
        centerTitle: true,
      );
    }
  }

  Widget _buildTextFields() {
    if (_form == FormType.login) {
      return new Container(
        child: new Column(
          children: <Widget>[
            new Container(
              child: new TextField(
                controller: _emailFilter,
                decoration: new InputDecoration(
                    labelText: 'Email'
                ),
              ),
            ),
            new Container(
              child: new TextField(
                controller: _passwordFilter,
                decoration: new InputDecoration(
                    labelText: 'Password'
                ),
                obscureText: true,
              ),
            )
          ],
        ),

      );
    } else {
      return new  Container(
        padding: EdgeInsets.all(8.0),
        child: new Column(
          children: <Widget>[
            new Container(
              child: new TextField(

                decoration: new InputDecoration(
                    labelText: 'Nombre Completo'
                ),
              ),
            ),
            new Container(
              child: new Column(
                children: <Widget>[
                  new Container(
                    child: new TextField(
                      controller: _emailFilter,
                      decoration: new InputDecoration(labelText: 'Email'),
                    ),
                  )
                ],
              ),
            ),
            new Container(
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new Container(
                      child: new TextField(
                        decoration: new InputDecoration(
                            labelText: 'Fecha de Nacimiento'
                        ),
                        enabled: false,
                        controller: TextEditingController(text: '$formattedDate'),

                      )
                  ),
                  new Container(
                    child: RaisedButton(
                      onPressed: () => _selectDate(context),
                      child: Icon(Icons.calendar_today),

                    ),

                  ),
                  new Container( child:

                  new Column (

                    children:

                    <Widget>[
                      new DropdownButton<String>(

                        hint: Text('Sexo'),
                        value: _selectedGender ,
                        items: _genders.map((gender){
                          return new DropdownMenuItem<String>(
                            value: gender,
                            child: new Text(gender),
                          );

                        }).toList(),
                        onChanged: (_) {
                          setState(() {
                            _selectedGender = (_);
                          });
                        },

                      )
                    ],
                  ),
                  ),
                  new Container(
                      child:
                      new Column(children: <Widget>[
                        new TextField(
                          decoration: new InputDecoration(labelText: 'Dirección'),
                        )
                      ],)
                  )

                ],
              ),

            ),
            new Container(
                child: new Column(
                  children: <Widget>[
                    new TextField(
                      decoration: new InputDecoration(labelText: 'Teléfono'),
                    )
                  ],
                )),
            new Container(
              child: new TextField(
                controller: _passwordFilter,
                decoration: new InputDecoration(
                    labelText: 'Password'
                ),
                obscureText: true,
              ),
            )
          ],
        ),

      );
    }
  }

  Widget _buildButtons() {
    if (_form == FormType.login) {
      return new Container(
        child: new Column(

          children: <Widget>[
            new RaisedButton(
              child: new Text('Login'),
              onPressed: _loginPressed,
            ),
            new FlatButton(
              child: new Text('¿Usuario Nuevo? Crea una cuenta.'),
              onPressed: _formChange,
            ),
            new FlatButton(
              child: new Text('Recuperar Contraseña'),
              onPressed: _passwordReset,
            )
          ],
        ),
      );
    } else {
      return new Container(
        child: new Column(
          children: <Widget>[
            new RaisedButton(
              child: new Text('Crear cuenta'),
              onPressed: _createAccountPressed,
            ),
            new FlatButton(
              child: new Text('¿Tienes cuenta? Iniciar Sesión.'),
              onPressed: _formChange,
            )
          ],
        ),
      );
    }
  }

  void _loginPressed () {
    print('The user wants to login with $_email and $_password');
  }

  void _createAccountPressed () {
    print('The user wants to create an accoutn with $_email and $_password');
    Navigator.push(context, MaterialPageRoute(builder: (context) => MyHomePage()),
    );

  }

  void _passwordReset () {
    print("The user wants a password reset request sent to $_email");
  }
}