import 'package:flutter/material.dart';

class Denuncia extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: ListView(
        children: <Widget>[
          Card(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                const ListTile(
                  leading: Icon(Icons.add_call, color: Colors.deepOrangeAccent, size: 25.0,),
                  title: Text("Denuncia por Llamada", style: TextStyle(fontStyle: FontStyle.normal, fontWeight: FontWeight.w700),),
                  subtitle: Text("Consabido, con el proposito de levantar un reporte.", style: TextStyle(fontStyle: FontStyle.italic, fontWeight: FontWeight.w400),),
                ),
                ButtonTheme.bar(
                  child: ButtonBar(
                    children: <Widget>[
                      FlatButton(
                        child: const Text("Hacer llamada"),
                        onPressed: () {
                          Navigator.push(context, MaterialPageRoute(builder: (context) => Call()));
                        },
                      )
                    ],
                  ),
                ),
                Divider(color: Colors.deepOrangeAccent, indent: 40.0, height: 10.0,)
              ],
            ),
          ),
          Card(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                const ListTile(
                  leading: Icon(Icons.location_searching, color: Colors.deepOrangeAccent, size: 25.0,),
                  title: Text("Denuncia por Ubicación", style: TextStyle(fontStyle: FontStyle.italic,fontWeight: FontWeight.w400),),
                  subtitle: Text("Anonimo, generar un reporte por medio de una ubicacion.", style: TextStyle(fontStyle: FontStyle.italic, fontWeight: FontWeight.w400),),
                ),
                ButtonTheme.bar(
                  child: ButtonBar(
                    children: <Widget>[
                      FlatButton(
                        child: const Text("Abrir Mapa"),
                        onPressed: () {
                          Navigator.push(context, MaterialPageRoute(builder: (context) => Map()));
                        },
                      )
                    ],
                  ),
                ),
                Divider(color: Colors.deepOrangeAccent, indent: 40.0, height: 10.0,)
              ],
            ),
          ),
        ],
      ),
    );
  }
}


class Call extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return Llamada();
  }
}

class Llamada extends State<Call>{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Denuncia por Llamada"),
        backgroundColor: Colors.cyan,
      ),
      body: Container(
        child: Center(
          child: Text("*Contactos para Denuncia y Input de Llamada*"),
        ),
      ),
    );
  }
}

class Map extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return Ubicacion();
  }
}

class Ubicacion extends State<Map>{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Denuncia por Ubicacion"),
        backgroundColor: Colors.cyan,
      ),
      body: Container(
        child: Center(
          child: Text("*Mapa en cuestion*"),
        ),
      ),
    );
  }
}