import 'package:flutter/material.dart';

class Serv extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Card(
        child: Center(
          child: new ContactList(_contacts),
        ),
      ),
    );
  }
}

  const _contacts = const <Contact> [
    const Contact(
      fullName: "Idea Geomatica", email: "ideaoG@example.com"),
    const Contact(
      fullName: "911", email: "911@example.com")
  ];


    class ContactList extends StatelessWidget{
      final List<Contact> _contacts;

      ContactList(this._contacts);
      @override
      Widget build(BuildContext context) {
        return new Scaffold(floatingActionButton:
          FloatingActionButton(
              child: Icon(Icons.group_add),onPressed: (){   }),
          floatingActionButtonLocation: FloatingActionButtonLocation.endDocked,
          body: ListView.builder(
            padding: new EdgeInsets.symmetric(vertical: 8.0),
            itemBuilder: (context, index) {
              return new _ContactListItem(_contacts[index]);
            },
            itemCount: _contacts.length,
          ),
        );
      }
    }

    class _ContactListItem extends ListTile {
      _ContactListItem(Contact contact)
          : super(
          title: new Text(contact.fullName),
          subtitle: new Text(contact.email),
          leading: new CircleAvatar(child: new Text(contact.fullName[0])));
    }

    class Contact {
      final String fullName;
      final String email;

      const Contact({this.fullName, this.email});
    }